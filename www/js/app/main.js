define(function (require) {
  var zepto = require('zepto');
  zepto(function($){
    // Setup FX Selection
    $("select#effect").on("change", function(e){
      var value = e.target.value;
      require(["./fx/"+value], function(fx){ fx(); });
    }).append((function(){
      var options = ['invert', 'grayscale', 'pixelize'];
      for (var i in options) {
        var node = document.createElement('option');
        node.value = options[i];
        node.text = options[i];
        options[i] = node;
      }
      return options;
    })()).trigger('change');
    // Setup Drag-and-Drop
    $(document).on("dragenter", function(e){
      e.stopPropagation();
      e.preventDefault();
    }).on("dragover", function(e){
      e.stopPropagation();
      e.preventDefault();
    }).on("drop", function(e){
      e.stopPropagation();
      e.preventDefault();
      var files = e.dataTransfer.files;
      if (files.length != 1) { return; }
      var file = files[0];
      if (!file.type.match(/image.*/)) { return; }
      var images = $("img#source");
      images.attr('file', file);
      var reader = new FileReader();
      reader.onload = function(e){ images.attr('src', e.target.result); }
      reader.readAsDataURL(file);
    });
  });
});
