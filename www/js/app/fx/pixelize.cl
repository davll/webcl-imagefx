const sampler_t sampler = CLK_NORMALIZED_COORDS_TRUE |
                          CLK_ADDRESS_CLAMP_TO_EDGE |
                          CLK_FILTER_NEAREST;

__kernel void main(__read_only image2d_t src,
                   __write_only image2d_t dst,
                   int block_size)
{
  int x = get_global_id(0), y = get_global_id(1);
  int2 dim = get_image_dim(dst);
  if (x >= dim.x || y >= dim.y)
    return;
  float tx = ((x/block_size)*block_size) / (float)dim.x;
  float ty = ((y/block_size)*block_size) / (float)dim.y;
  float4 color = read_imagef(src, sampler, (float2)(tx, ty));
  write_imagef(dst, (int2)(x, y), color);
}
