define([
  'zepto',
  './shared',
  'text!./invert.cl'
], function($, fx, code){
  
  function sourceImageElement()
  {
    return $("img#source")[0];
  }
  
  function sourceImageData()
  {
    var cav = document.createElement('canvas');
    var img = sourceImageElement();
    cav.width = img.width;
    cav.height = img.height;
    var ctx = cav.getContext('2d');
    ctx.drawImage(img, 0, 0);
    return ctx.getImageData(0, 0, img.width, img.height);
  }
  
  function outputCanvasElement()
  {
    return $("canvas#output")[0];
  }
  
  function execute()
  {
    _execute(fx.context, fx.commandQueue);
  }
  
  function _execute(ctx, cmd)
  {
    var src = sourceImageData();
    var format = { channelOrder: WebCL.CL_RGBA,
                   channelDataType: WebCL.CL_UNORM_INT8 }
    var imgIn = ctx.createImage2D(WebCL.CL_MEM_READ_ONLY, format, src.width, src.height, 0);
    var imgOut = ctx.createImage2D(WebCL.CL_MEM_WRITE_ONLY, format, src.width, src.height, 0);
    fx.dispatchWriteImage2D(imgIn, src.data);
    var kernel = fx.buildKernel(code);
    kernel.setKernelArg(0, imgIn);
    kernel.setKernelArg(1, imgOut);
    fx.dispatchExecution2D(kernel, src.width, src.height);
    var canvas = outputCanvasElement();
    canvas.width = src.width;
    canvas.height = src.height;
    var canvasCtx = canvas.getContext('2d');
    var result = canvasCtx.createImageData(src.width, src.height);
    fx.dispatchReadImage2D(imgOut, result.data);
    cmd.finish();
    canvasCtx.putImageData(result, 0, 0);
    imgIn.releaseCLResources();
    imgOut.releaseCLResources();
  }
  
  function setup()
  {
    $("#parameters-area").html('');
    $("button#execute").off().on("click", execute);
  }
  
  return setup;
});
