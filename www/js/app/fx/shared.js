define(function(){
  
  var context = (function(){
    var platform = WebCL.getPlatformIDs()[0];
    var props = [WebCL.CL_CONTEXT_PLATFORM, platform];
    var type = WebCL.CL_DEVICE_TYPE_GPU;
    return WebCL.createContextFromType(props, type);
  })();
  //
  var devices = context.getContextInfo(WebCL.CL_CONTEXT_DEVICES);
  var device = devices[0];
  var commandQueue = context.createCommandQueue(device, 0);
  //
  function buildKernel(code, entryName = 'main')
  {
    var program = context.createProgramWithSource(code);
    program.buildProgram(devices, "");
    return program.createKernel(entryName);
  }
  //
  function dispatchExecution2D(kernel, width, height, localSize = 16)
  {
    var globalWidth = Math.ceil(width / localSize) * localSize;
    var globalHeight = Math.ceil(height / localSize) * localSize;
    var lc = [localSize, localSize];
    var gb = [globalWidth, globalHeight];
    return commandQueue.enqueueNDRangeKernel(kernel, 2, [0,0], gb, lc, []);
  }
  //
  function dispatchReadImage2D(image, data)
  {
    var width = image.getImageInfo(WebCL.CL_IMAGE_WIDTH);
    var height = image.getImageInfo(WebCL.CL_IMAGE_HEIGHT);
    var o = [0,0,0]; // origin
    var d = [width, height, 1]; // dimensions
    commandQueue.enqueueReadImage(image, false, o, d, 0, 0, data, []);
  }
  //
  function dispatchWriteImage2D(image, data)
  {
    var width = image.getImageInfo(WebCL.CL_IMAGE_WIDTH);
    var height = image.getImageInfo(WebCL.CL_IMAGE_HEIGHT);
    var o = [0,0,0]; // origin
    var d = [width, height, 1]; // dimensions
    commandQueue.enqueueWriteImage(image, false, o, d, 0, 0, data, []);
  }
  
  //
  return {
    context: context,
    device: device,
    commandQueue: commandQueue,
    buildKernel: buildKernel,
    dispatchExecution2D: dispatchExecution2D,
    dispatchReadImage2D: dispatchReadImage2D,
    dispatchWriteImage2D: dispatchWriteImage2D
  };
});
